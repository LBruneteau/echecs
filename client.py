'''Client d'échecs
'''

import socket
import pickle
import chess


class Client(socket.socket):

    '''Classe client pour jouer et intéragir avec le serveur
    '''

    def __init__(self, ip_port: str) -> None:
        socket.socket.__init__(self, socket.AF_INET, socket.SOCK_STREAM)
        self.ip, port = ip_port.split(':')
        self.port: int = int(port)
        self.connect((self.ip, self.port))
        self.joueur: bool = bool(int(self.recv(1024).decode()))
        self.jeu: chess.Board = chess.Board()
        print('connecté à', self.ip, 'couleur', 'blanc' if self.joueur else 'noir')

    def send_recv(self, msg: str = 'get') -> None:
        '''Envoie le message au serveur et recoit le plateau

        Args:
            msg (str):Le message à encoder et envoyer
        '''
        self.send(msg.encode())
        self.jeu = pickle.loads(self.recv(4096*4))

    def main(self):
        run: bool = True
        while run:
            try:
                self.send_recv('get')
            except socket.error as e:
                print(e)
                break
            if self.jeu.is_game_over():
                print(self.jeu.result())
                break
            if self.jeu.turn == self.joueur:
                print(self.jeu)
                tour = input('Joue')
                self.send_recv(tour.encode())
                print(self.jeu)

if __name__ == "__main__":
    c = Client(input('serveur : '))
    c.main()