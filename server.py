'''
Serveur d'echecs
'''

import socket
import threading
import pickle
import chess

class Server(socket.socket):

    ''' Classe serveur, asynchrone
    '''

    def __init__(self):
        # pylint: disable=invalid-name
        socket.socket.__init__(self, socket.AF_INET, socket.SOCK_STREAM)
        self.ip: str = socket.gethostbyname(socket.gethostname() + '.local')
        self.bind((self.ip, 0))
        self.port: int = self.getsockname()[1]
        self.jeu: chess.Board = chess.Board()
        self.code: str = ':'.join((self.ip, str(self.port)))
        self.nb_joueurs = 0

    def client(self, conn: socket.socket, joueur: bool):
        '''echange avec le joueur
        '''
        msg = str(joueur).encode('UTF-8')
        conn.send(msg)
        reponse: str = ''
        while True:
            if self.jeu.is_game_over():
                print('Echec et mat')
            try:
                data: bytes = conn.recv(2048)
                reponse: str = data.decode()
                if not data or reponse == 'stop':
                    print('Déconnecté')
                    break
                if reponse != 'get':
                    print(f'Recu {reponse}')
                if reponse != 'get' and self.jeu.turn == joueur:
                    try:
                        self.jeu.push_uci(reponse)
                        print(self.jeu)
                    except ValueError:
                        try:
                            self.jeu.push_san(reponse)
                            print(self.jeu)
                        except Exception: pass
                conn.sendall(pickle.dumps(self.jeu))
            except Exception:
                break

    def client_async(self, conn: socket.socket, joueur: bool):
        '''client_async Le client mais asynchreon
        '''
        threading.Thread(target=self.client, args=(conn, joueur)).start()

    def main(self):
        '''lance le serv accepte les gens
        '''
        self.listen(2)
        joueur: int = 1
        for _ in range(2):
            conn, addr = self.accept()
            print('Connecté a', addr[0])
            self.client_async(conn, joueur)
            joueur -= 1
            self.nb_joueurs += 1
            
    def kill(self):
        try:
            self.shutdown(socket.SHUT_RDWR)
            self.close()
        except Exception:
            pass

def main():
    '''Lance le serveur
    '''
    serv = Server()
    threading.Thread(target=serv.main).start()
    print(serv.code)
    return serv

if __name__ == "__main__":
    main()
