from client import Client
import pygame
import chess
import chess.svg
import server
import os
import pyperclip
import pygame_menu as pgmenu
from bot import Bot

def stop():
    pygame.quit()
    os._exit(1)

theme = pgmenu.themes.THEME_DEFAULT.copy()
# theme.title_font = theme.widget_font = 'notosanslao'

class App:

    def __init__(self) -> None:
        self.ecran = pygame.display.set_mode((780, 780))
        self.client: Client = None
        self.case: str = ''
        pygame.display.set_caption('Echecs')
        pygame.display.set_icon(pygame.image.load('icone.png').convert_alpha())
        self.pieces = pygame.transform.scale(pygame.image.load('pieces.png').convert_alpha(), (540, 180))
        self.bg = None
        self.server: server.Server = None
        self.promotion = False

    def square_set(self, couleur: bool = None, local: bool = False) -> chess.SquareSet:
        if couleur is None:
            couleur = self.client.joueur
        squares = []
        try:
            if self.case and (self.jeu.color_at(chess.parse_square(self.case)) == couleur or (local)):
                for move in self.jeu.legal_moves:
                    if move.from_square == chess.parse_square(self.case):
                        squares.append(move.to_square)
        except Exception:
            pass
        return chess.SquareSet(squares)

    def case_cliquee(self, x: int, y: int, couleur: chess.Color) -> str:
        if x not in range(32, 749) or y not in range(32, 749):
            return ''
        x -= 32
        y -= 32
        if couleur:
            return 'abcdefgh'[x // 90] + str(range(8, -1, -1)[y // 90])
        return 'hgfedcba'[x // 90] + str(y // 90 + 1)

    def afficher_jeu(self, joueur: bool = None, local: bool = False) -> None:
        '''Dessine le plateau sur le fake display
        '''
        if not joueur:
            joueur = self.client.joueur
        types = (6, 5, 3, 2, 4, 1)
        dico = {0:7, 1:6, 2:5, 3:4, 4:3, 5:2, 6:1, 7:0}
        self.ecran.blit(self.bg, (0, 0))
        if self.jeu.is_check():
            case = self.jeu.king(self.jeu.turn)
            if joueur:
                case = 63 - case
            coord = list(reversed(divmod(case, 8)))
            for k in range(2):
                if not k:
                    coord[k] = dico[coord[k]]
                coord[k] *= 90
                coord[k] += 30
            pygame.draw.rect(self.ecran, (255, 0, 0), (coord[0], coord[1], 90, 90))
        try:
            move = self.jeu.peek()
        except IndexError:
            pass
        else:
            a, b = move.from_square, move.to_square
            if joueur:
                a, b = 63 - a, 63 - b
            ca, cb = list(reversed(divmod(a, 8))), list(reversed(divmod(b, 8)))
            for k in range(2):
                for c in (ca, cb):
                    if not k:
                        c[k] = dico[c[k]]
                    c[k] *= 90
                    c[k] += 30
            pygame.draw.rect(self.ecran, (120, 190, 32), (ca[0], ca[1], 90, 90))
            pygame.draw.rect(self.ecran, (120, 190, 32), (cb[0], cb[1], 90, 90))
        for case in range(64):
            if joueur:
                num = 63 - case
            else:
                num = case
            piece = self.jeu.piece_at(num)
            if piece is not None:
                coord = list(reversed(divmod(case, 8)))
                for k in range(2):
                    if not k:
                        coord[k] = dico[coord[k]]
                    coord[k] *= 90
                    coord[k] += 30
                rect = pygame.Rect(90 * types.index(piece.piece_type), 90 * (not piece.color), 90, 90)
                self.ecran.blit(self.pieces, coord, rect)
        for square in self.square_set(joueur, local):
            if joueur:
                square = 63 - square
            coord = list(reversed(divmod(square, 8)))
            for k in range(2):
                if not k:
                    coord[k] = dico[coord[k]]
                coord[k] *= 90
                coord[k] += 75
            pygame.draw.circle(self.ecran, (125, 125, 125, 50), coord, 15)
        if self.promotion:
            rect = pygame.Rect(90, 90 * (not (self.jeu.turn if local else joueur)), 4 * 90, 90)
            surf = pygame.Surface((360, 90))
            surf.fill((255, 255, 255))
            surf.blit(self.pieces, (0, 0), rect)
            self.ecran.blit(surf, (240, 240))


    def move_piece(self, x: int, y: int, couleur: chess.Color, local: bool = False) -> None:
        '''Change self.case

        Args:
            x (int): La coord x du click
            y (int): La coord y du click
        '''
        couleur_prom = self.jeu.turn if local else couleur
        case = self.case_cliquee(x, y, couleur)
        case_temp: str = self.case + case
        try:
            # verifier si légal
            if self.case and chess.Move.from_uci(case_temp) in self.jeu.legal_moves:
                self.case = case_temp
            # Vérifier si promotion
            elif case_temp[-1] in '18' and (self.jeu.color_at(chess.parse_square(self.case)) == couleur_prom) and (self.jeu.piece_at(chess.parse_square(self.case)).piece_type == chess.PAWN):
                self.promotion = True
                self.case = case_temp
            # Si pas valide on selectionne la case cliquée
            else:
                self.case = case
        except ValueError:
            # Si pas valide on selectionne la case cliquée
            self.case = case


    def envoyer_move(self):
        if self.jeu.turn == self.client.joueur:
            self.client.send_recv(self.case)
            if not self.promotion:
                self.case = ''

    def gerer_promotion(self, x: int, y: int) -> None:
        if pygame.Rect(240, 240, 4 * 90, 90).collidepoint((x, y)):
            if x in range(240, 330):
                self.case += 'q'
            elif x in range(330, 420):
                self.case += 'b'
            elif x in range(420, 510):
                self.case += 'n'
            else:
                self.case += 'r'
            self.promotion = False

    def mainloop(self):
        self.jeu = self.client.jeu
        self.bg = pygame.image.load('plateau.png' if self.client.joueur else 'plateau_reverse.png').convert()
        running = True
        checkmate = False
        self.promotion = False
        # font = pgmenu.font.get_font('kohinoorbangla', 80)
        while running:
            self.client.send_recv()
            self.jeu = self.client.jeu
            if self.jeu.is_game_over() and not checkmate:
                checkmate = True
                pygame.time.set_timer(pygame.USEREVENT, 5000)
            if self.server is not None and self.server.nb_joueurs == 1:
                self.server.kill()
                self.server = None
                pygame.time.set_timer(pygame.USEREVENT, 0)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    stop()
                if event.type == pygame.USEREVENT:
                    if self.server is not None:
                        self.server.kill()
                        self.server = None
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                    running = False
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    if self.server is not None:
                        self.server.kill()
                        self.server = None
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.promotion:
                        self.gerer_promotion(*event.pos)
                        self.envoyer_move()
                    else:
                        self.move_piece(*event.pos, self.client.joueur)
                        if len(self.case) == 4:
                            self.envoyer_move()
            self.afficher_jeu()
            if checkmate:
                render = font.render(self.jeu.result, True, (255, 255, 255))
                rect = render.get_rect()
                rect.center = (390, 390)
                self.ecran.blit(render, rect)
            pygame.display.flip()

    def rejoindre(self):
        menu = pgmenu.Menu('Rejoindre', 780, 780,  theme=theme)
        def joindre(_=None):
            try:
                self.client = Client(inp.get_value())
            except Exception:
                pass
            else:
                try:
                    self.mainloop()
                except Exception:
                    pass
                finally:
                    menu._back()
        def coller():
            inp._input_string = pyperclip.paste()
        inp = menu.add.text_input('Entrez le code : ', onreturn=joindre, maxchar=20)
        menu.add.button('Rejoindre', joindre)
        menu.add.button('Coller le code', coller)
        menu.add.button('Retour', pgmenu.events.BACK)
        return menu

    def creer(self):
        menu = pgmenu.Menu('Créer', 780, 780, onclose=pgmenu.events.BACK, theme=theme)
        self.server = server.main()
        self.client = Client(self.server.code)
        menu.add.label(f'code : {self.server.code}', selectable=True)
        menu.add.label('En attente du deuxième joueur')
        def retour():
            menu.disable()
            self.server.kill()
            self.server = None
        def debut():
            try:
                if self.server.nb_joueurs == 2:
                    menu.disable()
                    self.mainloop()
            except Exception:
                pass
        def copier():
            pyperclip.copy(self.server.code)
        menu.add.button('Démarrer', debut)
        menu.add.button('Copier le code', copier)
        menu.add.button('Retour', retour)
        menu.mainloop(self.ecran)

    def ordi(self):
        self.bg = pygame.image.load('plateau.png').convert()
        self.jeu = chess.Board()
        couleur = True
        bot = Bot(self.jeu, not couleur)
        running = True
        checkmate = False
        self.promotion = False
        # font = pgmenu.font.get_font('kohinoorbangla', 80)
        while running:
            if self.jeu.is_game_over() and not checkmate:
                checkmate = True
                pygame.time.set_timer(pygame.USEREVENT, 5000)
            elif self.jeu.turn != couleur and not self.jeu.is_game_over():
                #bot.jouer_random()
                bot.jouer_async()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    stop()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                if event.type == pygame.USEREVENT:
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    try:
                        if self.promotion:
                            self.gerer_promotion(*event.pos)
                            self.jeu.push_uci(self.case)
                        else:
                            self.move_piece(*event.pos, couleur)
                            if len(self.case) == 4:
                                self.jeu.push_uci(self.case)
                    except ValueError:
                        pass
            self.afficher_jeu(couleur)
            if checkmate:
                render = font.render(self.jeu.result, True, (255, 255, 255))
                rect = render.get_rect()
                rect.center = (390, 390)
                self.ecran.blit(render, rect)
            pygame.display.flip()

    def local(self):
        self.bg = pygame.image.load('plateau.png').convert()
        self.jeu = chess.Board()
        running = True
        checkmate = False
        # font = pgmenu.font.get_font('kohinoorbangla', 80)
        while running:
            if self.jeu.is_game_over() and not checkmate:
                checkmate = True
                pygame.time.set_timer(pygame.USEREVENT, 5000)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    stop()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                if event.type == pygame.USEREVENT:
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    try:
                        if self.promotion:
                            self.gerer_promotion(*event.pos)
                            self.jeu.push_uci(self.case)
                        else:
                            self.move_piece(*event.pos, True, True)
                            if len(self.case) == 4:
                                self.jeu.push_uci(self.case)
                    except ValueError:
                        pass
            self.afficher_jeu(True, True)
            if checkmate:
                render = font.render(self.jeu.result, True, (255, 255, 255))
                rect = render.get_rect()
                rect.center = (390, 390)
                self.ecran.blit(render, rect)
            pygame.display.flip()

    def menu(self):
        menu = pgmenu.Menu('Échecs', 780, 780, theme=theme)
        menu.add.button('Créer', self.creer)
        menu.add.button('Rejoindre', self.rejoindre())
        menu.add.button('Ordinateur', self.ordi)
        menu.add.button('Local', self.local)
        while True:
            try:
                events = pygame.event.get()
                for event in events:
                    if event.type == pygame.QUIT:
                        stop()
                if menu.is_enabled():
                    menu.update(events)
                    menu.draw(self.ecran)
                pygame.display.flip()
            except Exception as e:
                print(e)

def main():
    pygame.init()
    app = App()
    app.menu()

if __name__ == '__main__':
    main()
