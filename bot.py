import random
from typing import Tuple
import chess
import chess.polyglot
import threading

PION = 10
CHEVAL = 30
FOU = 30
TOUR = 50
DAME = 90
ROI = 900

TYPE_VAL = {chess.PAWN:PION, chess.KNIGHT:CHEVAL, chess.BISHOP:FOU, chess.ROOK:TOUR, chess.QUEEN:DAME, chess.KING:ROI}

pawnEvalWhite =[
        [0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0],
        [5.0,  5.0,  5.0,  5.0,  5.0,  5.0,  5.0,  5.0],
        [1.0,  1.0,  2.0,  3.0,  3.0,  2.0,  1.0,  1.0],
        [0.5,  0.5,  1.0,  2.5,  2.5,  1.0,  0.5,  0.5],
        [0.0,  0.0,  0.0,  2.0,  2.0,  0.0,  0.0,  0.0],
        [0.5, -0.5, -1.0,  0.0,  0.0, -1.0, -0.5,  0.5],
        [0.5,  1.0, 1.0,  -2.0, -2.0,  1.0,  1.0,  0.5],
        [0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]
    ]

pawnEvalBlack = list(reversed(pawnEvalWhite))

knightEval = [
        [-5.0, -4.0, -3.0, -3.0, -3.0, -3.0, -4.0, -5.0],
        [-4.0, -2.0,  0.0,  0.0,  0.0,  0.0, -2.0, -4.0],
        [-3.0,  0.0,  1.0,  1.5,  1.5,  1.0,  0.0, -3.0],
        [-3.0,  0.5,  1.5,  2.0,  2.0,  1.5,  0.5, -3.0],
        [-3.0,  0.0,  1.5,  2.0,  2.0,  1.5,  0.0, -3.0],
        [-3.0,  0.5,  1.0,  1.5,  1.5,  1.0,  0.5, -3.0],
        [-4.0, -2.0,  0.0,  0.5,  0.5,  0.0, -2.0, -4.0],
        [-5.0, -4.0, -3.0, -3.0, -3.0, -3.0, -4.0, -5.0]
    ]

bishopEvalWhite = [
    [ -2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -2.0],
    [ -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0],
    [ -1.0,  0.0,  0.5,  1.0,  1.0,  0.5,  0.0, -1.0],
    [ -1.0,  0.5,  0.5,  1.0,  1.0,  0.5,  0.5, -1.0],
    [ -1.0,  0.0,  1.0,  1.0,  1.0,  1.0,  0.0, -1.0],
    [ -1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0, -1.0],
    [ -1.0,  0.5,  0.0,  0.0,  0.0,  0.0,  0.5, -1.0],
    [ -2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -2.0]
]

bishopEvalBlack = list(reversed(bishopEvalWhite))

rookEvalWhite = [
    [  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0],
    [  0.5,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  0.5],
    [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
    [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
    [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
    [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
    [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
    [  0.0,   0.0, 0.0,  0.5,  0.5,  0.0,  0.0,  0.0]
]

rookEvalBlack = list(reversed(rookEvalWhite))

evalQueen = [
    [ -2.0, -1.0, -1.0, -0.5, -0.5, -1.0, -1.0, -2.0],
    [ -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0],
    [ -1.0,  0.0,  0.5,  0.5,  0.5,  0.5,  0.0, -1.0],
    [ -0.5,  0.0,  0.5,  0.5,  0.5,  0.5,  0.0, -0.5],
    [  0.0,  0.0,  0.5,  0.5,  0.5,  0.5,  0.0, -0.5],
    [ -1.0,  0.5,  0.5,  0.5,  0.5,  0.5,  0.0, -1.0],
    [ -1.0,  0.0,  0.5,  0.0,  0.0,  0.0,  0.0, -1.0],
    [ -2.0, -1.0, -1.0, -0.5, -0.5, -1.0, -1.0, -2.0]
]

kingEvalWhite = [

    [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
    [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
    [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
    [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
    [ -2.0, -3.0, -3.0, -4.0, -4.0, -3.0, -3.0, -2.0],
    [ -1.0, -2.0, -2.0, -2.0, -2.0, -2.0, -2.0, -1.0],
    [  2.0,  2.0,  0.0,  0.0,  0.0,  0.0,  2.0,  2.0 ],
    [  2.0,  3.0,  1.0,  0.0,  0.0,  1.0,  3.0,  2.0 ]
]

kingEvalBlack = list(reversed(kingEvalWhite))

pawntable = [
    0, 0, 0, 0, 0, 0, 0, 0,
    5, 10, 10, -20, -20, 10, 10, 5,
    5, -5, -10, 0, 0, -10, -5, 5,
    0, 0, 0, 20, 20, 0, 0, 0,
    5, 5, 10, 25, 25, 10, 5, 5,
    10, 10, 20, 30, 30, 20, 10, 10,
    50, 50, 50, 50, 50, 50, 50, 50,
    0, 0, 0, 0, 0, 0, 0, 0]

knightstable = [
    -50, -40, -30, -30, -30, -30, -40, -50,
    -40, -20, 0, 5, 5, 0, -20, -40,
    -30, 5, 10, 15, 15, 10, 5, -30,
    -30, 0, 15, 20, 20, 15, 0, -30,
    -30, 5, 15, 20, 20, 15, 5, -30,
    -30, 0, 10, 15, 15, 10, 0, -30,
    -40, -20, 0, 0, 0, 0, -20, -40,
    -50, -40, -30, -30, -30, -30, -40, -50]

bishopstable = [
    -20, -10, -10, -10, -10, -10, -10, -20,
    -10, 5, 0, 0, 0, 0, 5, -10,
    -10, 10, 10, 10, 10, 10, 10, -10,
    -10, 0, 10, 10, 10, 10, 0, -10,
    -10, 5, 5, 10, 10, 5, 5, -10,
    -10, 0, 5, 10, 10, 5, 0, -10,
    -10, 0, 0, 0, 0, 0, 0, -10,
    -20, -10, -10, -10, -10, -10, -10, -20]

rookstable = [
    0, 0, 0, 5, 5, 0, 0, 0,
    -5, 0, 0, 0, 0, 0, 0, -5,
    -5, 0, 0, 0, 0, 0, 0, -5,
    -5, 0, 0, 0, 0, 0, 0, -5,
    -5, 0, 0, 0, 0, 0, 0, -5,
    -5, 0, 0, 0, 0, 0, 0, -5,
    5, 10, 10, 10, 10, 10, 10, 5,
    0, 0, 0, 0, 0, 0, 0, 0]

queenstable = [
    -20, -10, -10, -5, -5, -10, -10, -20,
    -10, 0, 0, 0, 0, 0, 0, -10,
    -10, 5, 5, 5, 5, 5, 0, -10,
    0, 0, 5, 5, 5, 5, 0, -5,
    -5, 0, 5, 5, 5, 5, 0, -5,
    -10, 0, 5, 5, 5, 5, 0, -10,
    -10, 0, 0, 0, 0, 0, 0, -10,
    -20, -10, -10, -5, -5, -10, -10, -20]

kingstable = [
    20, 30, 10, 0, 0, 10, 30, 20,
    20, 20, 0, 0, 0, 0, 20, 20,
    -10, -20, -20, -20, -20, -20, -20, -10,
    -20, -30, -30, -40, -40, -30, -30, -20,
    -30, -40, -40, -50, -50, -40, -40, -30,
    -30, -40, -40, -50, -50, -40, -40, -30,
    -30, -40, -40, -50, -50, -40, -40, -30,
    -30, -40, -40, -50, -50, -40, -40, -30]

class Bot:

    def __init__(self, jeu: chess.Board, couleur: chess.Color) -> None:
        self.jeu = jeu
        self.couleur = couleur
        self.fonct = max if self.couleur else min
        self.running = False

    def move_random(self) -> chess.Move:
        return random.choice(list(self.jeu.legal_moves))

    def jouer_move(self, move: chess.Move) -> None:
        if self.jeu.turn == self.couleur:
            self.jeu.push(move)

    def jouer_random(self) -> None:
        self.jouer_move(self.move_random())

    def eval_jeu(self) -> int:
        def valeur(case: int):
            piece = self.jeu.piece_at(case)
            if piece is None:
                return 0
            val = TYPE_VAL[piece.piece_type]
            if piece.color == chess.WHITE:
                if piece.piece_type == chess.PAWN:
                    val += pawnEvalWhite[case//8][case%8]
                if piece.piece_type == chess.KNIGHT:
                    val += knightEval[case//8][case%8]
                if piece.piece_type == chess.BISHOP:
                    val += bishopEvalWhite[case//8][case%8]
                if piece.piece_type == chess.ROOK:
                    val += rookEvalWhite[case//8][case%8]
                if piece.piece_type == chess.QUEEN:
                    val += evalQueen[case//8][case%8]
                if piece.piece_type == chess.KING:
                    val += kingEvalWhite[case//8][case%8]
            if piece.color == chess.BLACK:
                if piece.piece_type == chess.PAWN:
                    val += pawnEvalBlack[case//8][case%8]
                if piece.piece_type == chess.KNIGHT:
                    val += knightEval[case//8][case%8]
                if piece.piece_type == chess.BISHOP:
                    val += bishopEvalBlack[case//8][case%8]
                if piece.piece_type == chess.ROOK:
                    val += rookEvalBlack[case//8][case%8]
                if piece.piece_type == chess.QUEEN:
                    val += evalQueen[case//8][case%8]
                if piece.piece_type == chess.KING:
                    val += kingEvalBlack[case//8][case%8]
                val *= -1
            return val
        nb = 0
        for case in range(64):
            nb  += valeur(case)
        return nb

    def evaluer(self, move: chess.Move) -> int:
        self.jeu.push(move)
        score = self.eval_jeu()
        self.jeu.pop()
        return score

    def meilleur_0(self) -> Tuple[chess.Move, int]:
        '''Retourne le meilleur move à profondeur 1
        '''
        moves = list(self.jeu.legal_moves)
        meilleur_move = moves[0]
        meilleur_score = self.evaluer(meilleur_move)
        fonct = max if self.jeu.turn == chess.WHITE else min
        for move in moves[1:]:
            score = self.evaluer(move)
            if fonct((score, meilleur_score)) == score:
                meilleur_move = move
                meilleur_score = score
        return meilleur_move, meilleur_score

    def minimax(self, profondeur: int, alpha, beta) -> chess.Move:
        if not profondeur:
            return self.eval_jeu()
        if self.jeu.is_checkmate():
            if self.jeu.turn:
                return -99999999999
            return 9999999999
        if self.jeu.is_game_over():
            return 0
        fonct = max if self.jeu.turn == chess.WHITE else min
        score = -9999999999999 if self.jeu.turn else 9999999999999
        for move in self.jeu.legal_moves:
            self.jeu.push(move)
            score = fonct(self.minimax(profondeur-1, alpha, beta), score)
            self.jeu.pop()
            if self.jeu.turn:
                alpha = max(alpha, score)
                if beta <= alpha:
                    break
            else:
                beta = min(beta, score)
                if beta <= alpha:
                    break
        return score

    def mieux(self, profondeur):
        try:
            return chess.polyglot.MemoryMappedReader("human.bin").weighted_choice(self.jeu).move
        except Exception:
            moves = list(self.jeu.legal_moves)
            scores = []
            for move in moves:
                self.jeu.push(move)
                scores.append(self.minimax(profondeur, -999999999999999, 999999999999))
                self.jeu.pop()
            fonct = max if self.jeu.turn == chess.WHITE else min
            best = fonct(scores)
            if scores.count(best) == 1:
                return moves[scores.index(best)]
            return random.choice([moves[i] for i in range(len(moves)) if scores[i] == best])

    def jouer(self):
        jeu = self.jeu
        self.jeu = self.jeu.copy()
        self.running = True
        jeu.push(self.mieux(2))
        self.jeu = jeu
        self.running = False

    def jouer_async(self):
        if not self.running:
            t = threading.Thread(target=self.jouer).start()